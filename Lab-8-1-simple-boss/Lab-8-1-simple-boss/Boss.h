#ifndef BOSS_H
#define BOSS_H
#include "Enemy.h"
using namespace std;

class Boss : public Enemy
{
public:
	int m_DamageMultiplier;

	Boss();
	void SpecialAttack() const;

};
#endif