#ifndef CRITTER_H
#define CRITTER_H
#include <iostream>
using namespace std;

class Critter
{
public:
	Critter(int hunger = 0, int boredom = 0,int age = 0);
	void Talk();
	void Eat(int food = 4);
	void Play(int fun = 4);
	void Sleep();
	void myAgeIs();

private:
	int m_Hunger;
	int m_Boredom;
	int m_age;
	void getAge();
	int GetMood() const;
	void PassTime(int time = 1);

};


#endif


