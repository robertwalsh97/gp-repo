
#include "Critter.h"





Critter::Critter(int hunger, int boredom, int age) :
	m_Hunger(hunger),
	m_Boredom(boredom),
	m_age(age)
{}

void Critter::getAge()
{
	
	if (m_age > 70)
	{
		cout << "Your critter has died of old age. Rest in Peace Critter...";
	}
	else {
		cout << "Your critter's age is " << m_age << endl;
	}
}

inline int Critter::GetMood() const
{
	return (m_Hunger + m_Boredom);
}

void Critter::PassTime(int time)
{
	m_Hunger += time;
	m_Boredom += time;
	m_age += time;
}

void Critter::Talk()
{
	if (m_age > 70)
	{
		cout << "Your critter has died of old age. Rest in Peace Critter...";
	}
	
	else 
	{
		cout << "I'm a critter and I feel ";

		int mood = GetMood();
		if (mood > 15)
		{
			cout << "mad.\n";
		}
		else if (mood > 10)
		{
			cout << "frustrated.\n";
		}
		else if (mood > 5)
		{
			cout << "okay.\n";
		}
		else
		{
			cout << "happy.\n";
		}

		PassTime();
	}
}

void Critter::Eat(int food)
{
	if (m_age > 70)
	{
		cout << "Your critter has died of old age. Rest in Peace Critter...";
	}

	else
	{
		cout << "Brruppp.\n";

		m_Hunger -= food;
		if (m_Hunger < 0)
		{
			m_Hunger = 0;
		}

		PassTime(2);
	}
}

void Critter::Play(int fun)
{
	if (m_age > 70)
	{
		cout << "Your critter has died of old age. Rest in Peace Critter...";
	}

	else
	{
		cout << "Wheee!\n";

		m_Boredom -= fun;
		if (m_Boredom < 0)
		{
			m_Boredom = 0;
		}

		PassTime(3);
	}
}

void Critter::Sleep()
{
	if (m_age > 70)
	{
		cout << "Your critter has died of old age. Rest in Peace Critter...";
	}
	
	else
	{
		cout << "*Snores loudly*\n";
		PassTime(5);
	}
}

void Critter::myAgeIs()
{
	getAge();
}
