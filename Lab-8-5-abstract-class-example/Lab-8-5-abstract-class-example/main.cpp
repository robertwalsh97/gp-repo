#include "troll.h"

int main()
{
	Creature* pCreature = new Orc();
	pCreature->Greet();
	pCreature->DisplayHealth();

	Creature* pCreature2 = new Troll();
	pCreature2->Greet();
	pCreature2->DisplayHealth();

	getchar();
	return 0;
}
