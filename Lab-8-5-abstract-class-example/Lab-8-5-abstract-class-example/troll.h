#include "orc.h"

class Troll : public Creature
{
public:
	Troll(int health = 200);
	virtual void Greet() const;
};
