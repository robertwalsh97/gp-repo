#include "troll.h"

Troll::Troll(int health) :
	Creature(health)
{}

void Troll::Greet() const
{
	cout << "\nThe troll grunts hello.\n";
}
