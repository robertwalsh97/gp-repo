#include "Boss.h"

int main()
{
	cout << "Calling Attack() on Boss object through pointer to Enemy:\n";
	Enemy* pBadGuy = new Boss();
	pBadGuy->Attack();

	cout << "\n\nDeleting pointer to Enemy:\n";
	delete pBadGuy;
	pBadGuy = 0;

	cout << "\n\nCalling Attack() on Boss object through pointer to Enemy AnotherBadGuy:\n";
	Enemy* pAnotherBadGuy = new Boss();
	pAnotherBadGuy->Attack();

	cout << "\n\nDeleting pointer to Enemy AnotherBadGuy:\n";
	delete pAnotherBadGuy;
	pAnotherBadGuy = 0;

	getchar();
	return 0;
}
