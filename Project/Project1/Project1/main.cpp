#include <iostream>
#include <string>
#include <vector>

using namespace std;

void checkWord(string playerWord, vector<string> words);

int main()
{
	string playerWord;
	vector<string> words;
	words.push_back("END");
	words.push_back("SEND");
	words.push_back("BEND");
	words.push_back("PRETEND");

	cout << "Welcome to Wordthings! Enter EXIT at any point to quit. \n\nA battle is surely brewing!\nFIGHT!\n\n";
  //cout << "123456789\n";
	cout << "     yg    \n";
	cout << "    beeh    \n";
	cout << "   triedj   \n";
	cout << "  cse  dof  \n";
	cout << " endbendjxp \n";
	cout << "xfpretendnqq\n\n";

	while (playerWord != "EXIT")
	{
		cout << "Hello there FRIEND...\n\n";
		cin >> playerWord;
		
		for (int i = 0; i < playerWord.size(); i++) 
		{
			playerWord[i]= toupper(playerWord[i]);
		}
		
		checkWord(playerWord, words);
	}

	getchar();
	return 0;
}

void checkWord(string playerWord, vector<string>words)
{
	if (find(words.begin(), words.end(), playerWord)!=words.end())
	{
		cout << "You use " << playerWord << "! It's super effective!\n";
	}
	else
	{
		cout << "Pfft, never heard of that word.\n";
	}
}