/* Loop the game until a win or lose condition is met. 
Execute all the functions in order until that point*/

#include "Game.h"
#include <time.h>

int main()
{
	srand((time(NULL))); //Creates random number generator with time as the seed
	bool StopGame = false; //Used to determine when the game is over
	int i = 0; //Use for turns

	Game* Game1 = new Game(); //Creates game object

	Game1->init(); //Executes init function with Game object

	while (StopGame==false) //Loops program functions until a condition is met that ends the game (i.e player dies or defeats enough enemies)
	{
		if (i == 0)
		{
			cout << "Turn 1. Let the games begin!\n\n";
		}
		else 
		{
			cout << "Turn " << i+1 << ".\n\n"; //Print what number turn it is
		}

		Game1->draw();  //Prints ASCII map 
		Game1->update(); //Virtual function used by the player and enemies to move
		StopGame = Game1->battle(); //Battle includes all battle events. Save the return value of battle() to determine if the game is over
		//Game1->info(); //Prints stats of player and enemies. Useful for bug fixing but too obtrusive otherwise, hence it is commented out
		Game1->clean(); //Removes game objects from the game object vector when their health is less than or equal to 0
	}

	//Game1->info();

	system("pause");//Requires user input to exit program
}