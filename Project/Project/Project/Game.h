/*Create a Game Class which has 1 player and 4 enemy
pointer objects and a list of pointers to GameObjects*/

#ifndef GAME_H
#define GAME_H
#include "Player.h"
#include "Enemy.h"
#include "NPC.h"

class Game
{
public:
	Game(); //Constructor
	~Game(); //Destructor
	vector<GameObject*> m_vpGameObjects; //Vector of Game Object pointers
	vector<int*> randNums;	//Vector of random numbers
	vector<int> enemyStats; //Vector for storing enemy stats
	vector<string> words; //Vector to store words that can be rhmyed with for each enemy variation
	vector<vector<string>> wordList; //Vector to store vector of words above
	string playerWord; //String used to compare player input with words
	
	//To beat the game you must beat three of each enemy type. Therefore a counter is needed for each enemy type
	int treyesEncountered; //Counter for the level 1 enemy - Treye
	int spherosEncountered; //Counter for the level 2 enemy - Sphero
	int rubicksEncountered; //Counter for the level 3 enemy - Rubicks

	void init(); //Prints story, adds words to word vectors and creates game objects

	void draw(); // Creates ASCII map

	void update(); //Member function. Cycle through the enemy and player objects and call the update function for each object

	bool battle(); // Contains all the different battle behaviours

	void info(); // Cycle through the enemy and player objects and call the info function for each object

	void clean(); //remove any objects from the list whose health is 0 or less
	
	int checkWord(string playerWord, vector<string>words); //Compare the player input with existing usable words
	
	vector<int> createNums(int whichEnemy); //Generate random numbers for enemy co-ordinates, health and speed
	
	vector<string> enemyVar(int whichEnemy); //Checks what enemy has been encountered ,selects the correct word vector and prints the corresponding quote and ASCII art

	void printEnemy(int whichEnemy); //Contains all the enemy ASCII art and corresponding quotes. Prints the appropriate ones to screen
};

#endif // !GAME_H