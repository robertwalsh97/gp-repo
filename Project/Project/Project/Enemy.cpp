/*Create an Enemy class that inherits from GameObject and defines 
its update function to increment / decrement the x and y coords based 
on its speed).If they hit a perimeter, they have to change direction. */

#include "Enemy.h"

Enemy::Enemy()
{
}

Enemy::~Enemy()
{
}

void Enemy::update() //Creates rand numbers to determine where the enemy moves on the map
{
	//Random num to determine what direction the enemy goes
	int enemyMoveNum = rand();
	int enemyMove = (enemyMoveNum % 5);

	if (enemyMove == 0)
	{
		//increment y axis
		if ((m_y + m_speed < 30))
		{
			m_y += m_speed;
		}
		else
		{
			update();
		}
	}

	else if (enemyMove == 1)
	{
		//decrement x axis
		if ((m_x - m_speed >= 0))
		{		
			m_x -= m_speed;
		}

		else
		{
			update();
		}
	}

	else if (enemyMove == 2)
	{
		//decrement y axis
		if (m_y - m_speed >= 0) 
		{
			m_y -= m_speed;
		}
		else
		{
			update();
		}
		
	}

	else if (enemyMove == 3)
	{
		//increment x axis
		if (m_x + m_speed < 30)
		{
			m_x += m_speed;
		}
		else
		{
			update();
		}
	}

	else
	{
		//Don't move
	}
}