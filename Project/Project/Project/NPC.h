#ifndef NPC_H
#define NPC_H
#include "GameObject.h"

class NPC : public GameObject
{
public:
	NPC(); //Constructor
	~NPC(); //Destructor

	void update();
};

#endif // !NPC_H