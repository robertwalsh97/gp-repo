/*Create a Player class that inherits from GameObject 
and defines its update function to print to the screen 
�that it requires user input to determine x and y coords�
and then increment / decrement the x and y coords based on
its speed and deduct from health(character speed * 2).*/
#include "Player.h"

Player::Player()
{
}

Player::~Player()
{
}

void Player::update() //Takes in user input to determine where the player moves on the map
{
	char playerInput;
	cout << "\n\nPlayer... make your move! Use the WASD keys to move- W = up, A = left, S = down, D = right\n";
	cin >> playerInput; //Read from keyboard and save to playerInput
	playerInput = toupper(playerInput); //Capitalise letter entered

	if (playerInput == 'W')
	{
		//increment y axis
		if ((m_y - m_speed >= 0))
		{
			m_y -= m_speed;
			cout << "\n\nBoom! You moved north! Wow, you really wouldn't see that in any other game!";
		}
		else
		{
			cout << "Yup, you just hit a wall. Maybe try going another way?\n";
			update();
		}
	
	}
	else if (playerInput == 'A')
	{
		//decrement x axis
		if(m_x - m_speed >= 0)
		{
			m_x -= m_speed;
			cout << "\n\nWow! You moved west! Congratulations!";
		}
		else
		{
			cout << "Yup, you just hit a wall. Maybe try going another way?\n";
			update();
		}

	}
	else if (playerInput == 'S')
	{
		//decrement y axis
		if(m_y + m_speed < 30)
		{
			m_y += m_speed;
			cout << "\n\nHoly cannoli! This is going south fast!";
		}
		else
		{
			cout << "Yup, you just hit a wall. Maybe try going another way?\n";
			update();
		}
	}
	else if (playerInput == 'D')
	{
		//increment x axis
		if(m_x + m_speed < 30)
		{
			m_x += m_speed;
			cout << "\n\nYou head eastward! What epic encounters await, one can only wonder...";
		}
		else
		{
			cout << "Yup, you just hit a wall. Maybe try going another way?\n";
			update();
		}
	}
	else
	{
		cout << "\n\nInvalid input... Reading is hard, am I right?";
		update(); //Return to input prompt
	}
}