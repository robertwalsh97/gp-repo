﻿#include "Game.h"
#include <ctime>

//Default constructor and destructor
Game::Game()
{
}
Game::~Game()
{
}
	
//Prints story, adds words to word vectors and creates game objects
void Game::init()
{
	//Print the story to screen
	cout << "Welcome adventurer, to the Rhymesters Rehabilitation Centre! You have had some incredible adventures in the past,\n"; 
	cout << "but they seem to have gotten the best of you. You were once a great warrior, feared and respected throughout the land,\n";
	cout << "but now you are merely a nameless peon, with some mild PTSD symptom. That is why you are here, to get back on track\n";
	cout << "and rediscover yourself through your love of battle. Therefore, we have provided a number of monsters for you to defeat\n";
	cout << "and an enclosed area for you to explore. Once you have beaten three of each enemy type, you can leave the centre, and return to\n";
	cout << "the land of slaying dragons and rescuing princesses. So get out there and have fun, but do not forget, the key to\n";
	cout << "defeating these monsters is to rhyme with them. That is why they are called Rhymesters after all!\n";

	//Add words to the vector
	//Treye v1
	words.push_back("END");
	words.push_back("SEND");
	words.push_back("BEND");
	words.push_back("TREND");
	words.push_back("PRETEND");
	wordList.push_back(words); //Add this set of words to the vector of words
	//Treye v2
	words.push_back("OF");
	words.push_back("DOVE");
	words.push_back("ABOVE");
	words.push_back("GLOVE");
	words.push_back("SHOVE");
	wordList.push_back(words);
	//Treye v3
	words.push_back("DUG");
	words.push_back("DRUG");
	words.push_back("THUG");
	words.push_back("SHRUG");
	words.push_back("UNPLUG");
	wordList.push_back(words);
	//Treye v4
	words.push_back("TONE");
	words.push_back("BONE");
	words.push_back("CLONE");
	words.push_back("SCONE");
	words.push_back("PHONE");
	wordList.push_back(words);
	//Treye v5
	words.push_back("BUFF");
	words.push_back("PUFF");
	words.push_back("ROUGH");
	words.push_back("TOUGH");
	words.push_back("BLUFF");
	wordList.push_back(words);

	//Sphero v1
	words.push_back("BELL");
	words.push_back("FELL");
	words.push_back("SELL");
	words.push_back("DWELL");
	words.push_back("SMELL");
	wordList.push_back(words);
	//Sphero v2
	words.push_back("BEE");
	words.push_back("FREE");
	words.push_back("FLEE");
	words.push_back("AGREE");
	words.push_back("SPREE");
	wordList.push_back(words);
	//Sphero v3
	words.push_back("DIVE");
	words.push_back("HIVE");
	words.push_back("FIVE");
	words.push_back("DRIVE");
	words.push_back("THRIVE");
	wordList.push_back(words);
	//Sphero v4
	words.push_back("MEAN");
	words.push_back("SEEN");
	words.push_back("GREEN");
	words.push_back("CLEAN");
	words.push_back("SCREEN");
	wordList.push_back(words);
	//Sphero v5
	words.push_back("BLUE");
	words.push_back("CHEW");
	words.push_back("GLUE");
	words.push_back("SCREW");
	words.push_back("QUEUE");
	wordList.push_back(words);

	//Rubicks v1
	words.push_back("DARE");
	words.push_back("CARE");
	words.push_back("HAIR");
	words.push_back("FLARE");
	words.push_back("GLAIR");
	wordList.push_back(words);
	//Rubicks v2
	words.push_back("CRY");
	words.push_back("SHY");
	words.push_back("SIGH");
	words.push_back("THIGH");
	words.push_back("REPLY");
	wordList.push_back(words);
	//Rubicks v3
	words.push_back("BASE");
	words.push_back("CASE");
	words.push_back("RACE");
	words.push_back("CHASE");
	words.push_back("PLACE");
	wordList.push_back(words);
	//Rubicks v4
	words.push_back("BANE");
	words.push_back("GAIN");
	words.push_back("RAIN");
	words.push_back("CHAIN");
	words.push_back("SLAIN");
	wordList.push_back(words);
	//Rubicks v5
	words.push_back("VEST");
	words.push_back("WEST");
	words.push_back("TEST");
	words.push_back("QUEST");
	words.push_back("GUEST");
	wordList.push_back(words);
	//words.clear();

	//---------PLAYER-----------
	//Create player object and assign set values for TypeID, Max Health, Speed, X and Y coordinates, attack power, defence and luck
	Player* pPlayer = new Player(); //Create new object of type Player
	pPlayer->spawn("Player", 200, 1, 15, 29, 20, 5, 1); //Use spawn function to assign stats to player

	//Push new objects onto m_vpGameObjects vector
	m_vpGameObjects.push_back(pPlayer);

	//----------NPC's-----------
	//Healer
	for (int i1 = 1; i1 < 7; i1++) //Generate 6 healers
	{
		string IDnum = to_string(i1); //Turn the incrementor to a string to be appended to the object name, giving each object a unique name
		NPC* pHealer = new NPC();
		enemyStats = createNums(0);	//Set values in vector equal to return values of the createNums function. Pass in 0 so the NPC is only given random X and Y coordinates. They have no need for health, luck etc.
		pHealer->spawn("Healer " + IDnum, 1, 0, enemyStats[0], enemyStats[1], 0, 0, 0); //Append ID Num to string to create a unique TypeID. Pass 0 into stats NPC's don't need

		enemyStats.clear(); //Clear vector as it needs to be used with the next object
		m_vpGameObjects.push_back(pHealer);
	}

	//Linguist
	for (int i2 = 1; i2 < 6; i2++)
	{
		string IDnum = to_string(i2);
		NPC* pLinguist = new NPC();
		enemyStats = createNums(0);
		pLinguist->spawn("Linguist " + IDnum, 1, 0, enemyStats[0], enemyStats[1], 0, 0, 0);

		enemyStats.clear();
		m_vpGameObjects.push_back(pLinguist);
	}

	//Blacksmith
	for (int i3 = 1; i3 < 6; i3++)
	{
		string IDnum = to_string(i3);
		NPC* pBlacksmith = new NPC();
		enemyStats = createNums(0);	//Set values in vector equal to return values of the createNums function
		pBlacksmith->spawn("Blacksmith " + IDnum, 1, 0, enemyStats[0], enemyStats[1], 0, 0, 0);

		enemyStats.clear();
		m_vpGameObjects.push_back(pBlacksmith);
	}

	//Armorer
	for (int i4 = 1; i4 < 6; i4++)
	{
		string IDnum = to_string(i4);
		NPC* pArmorer = new NPC();
		enemyStats = createNums(0);	//Set values in vector equal to return values of the createNums function
		pArmorer->spawn("Armorer " + IDnum, 1, 0, enemyStats[0], enemyStats[1], 0, 0, 0);

		enemyStats.clear();
		m_vpGameObjects.push_back(pArmorer);
	}

	//---------ENEMIES----------
	//Create Enemy pointer object and assign values from the createNums function for stats
	//Level 1 enemy: Treye
	for (int i = 1; i < 6; i++)
	{
		string IDnum = to_string(i);
		Enemy* pTreye = new Enemy();
		enemyStats = createNums(1);	//Set values in vector equal to return values of the createNums function. Pass in 1 so the function knows the enemy Treye is to be used
		pTreye->spawn("Treye " + IDnum, enemyStats[2], 1, enemyStats[0], enemyStats[1], enemyStats[3], enemyStats[4], enemyStats[5]); //Use returned values of the enemyStats vector for randomised stats

		//Reset vector and refill with random numbers
		enemyStats.clear();

		m_vpGameObjects.push_back(pTreye);
	}
	//Level 2 enemy: Sphero
	for (int j = 1; j < 6; j++)
	{
		string IDnum = to_string(j);
		Enemy* pSphero = new Enemy();
		enemyStats = createNums(2);
		pSphero->spawn("Sphero " + IDnum, enemyStats[2], 1, enemyStats[0], enemyStats[1], enemyStats[3], enemyStats[4], enemyStats[5]);

		enemyStats.clear();

		m_vpGameObjects.push_back(pSphero);
	}

	//Level 3 enemy: Rubicks
	for (int k = 1; k < 6; k++)
	{
		string IDnum = to_string(k);
		Enemy* pRubicks = new Enemy();
		enemyStats = createNums(3);
		pRubicks->spawn("Rubicks " + IDnum, enemyStats[2], 1, enemyStats[0], enemyStats[1], enemyStats[3], enemyStats[4], enemyStats[5]);

		enemyStats.clear();

		m_vpGameObjects.push_back(pRubicks);
	}

}

void Game::draw() //Creates ASCII map
{
	//Use iterator to search through the vector
	vector <GameObject*>::const_iterator iter;

	//Run the draw function in game object for each game object
	for (iter = m_vpGameObjects.begin(); iter != m_vpGameObjects.end(); iter++)
	{
		(*iter)->draw();
	}

	cout << "\nLEGEND\nP = Player | H = Healer | L = Linguist | B = Blacksmith | A = Armorer | T = Treye (Level 1 Enemy) | S = Sphero (Level 2 Enemy) | R = Rubicks (Level 3 Enemy)\n\n";
	//Player y = j | Player x = i
	for (int j = 0; j < 30; j++) //Print columns
	{
		for (int i = 0; i < 30; i++) //Print rows
		{		
			//Check each game object's x and y coordinates and compare with the above incrementors to determine when to print the game object to the map
			for (iter = m_vpGameObjects.begin(); iter != m_vpGameObjects.end(); iter++)
			{
				if ((*iter)->returnX() == i && (*iter)->returnY() == j)
				{
					cout << (*iter)->returnType()[0] << " ";
					i++;
				}	
			}
			if (i < 30)
			{
				cout << ". "; 
			}
		}
		cout << "\n";
	}
}

void Game::update() //Runs update function from GameObject.cpp for each object
{
	vector <GameObject*>::const_iterator iter;

	for (iter = m_vpGameObjects.begin(); iter != m_vpGameObjects.end(); iter++)
	{
		(*iter)->update();
	}
}

bool Game::battle()
{
	vector <GameObject*>::const_iterator iter;

	int pX;
	int pY;

	//Use getter method to save to local variabes for comparison 
	pX = m_vpGameObjects.front()->returnX();
	pY = m_vpGameObjects.front()->returnY();
	
	for (iter = m_vpGameObjects.begin(); iter != m_vpGameObjects.end(); iter++)//Search through game object vector
	{
		//If game objects name is not the player (i.e. enemy) and X/Y coordinates are equal 
		if (((*iter)->returnType() != "Player") && (pX == (*iter)->returnX()) && (pY == (*iter)->returnY())) 
		{
			if ((*iter)->returnType()[0] == 'H') //If the first letter of the string is a 'H', it is a Healer
			{
				cout << "\nYou encounter a Healer. Your health has been restored and your max health increased by 50!\n";
				(*iter)->GameObject::removeNPC();
				m_vpGameObjects.front()->GameObject::HealerBehaviour(); //Run set method with the player object to add stats to the player
			}

			else if ((*iter)->returnType()[0] == 'L') //If the first letter of the string is an 'L', it is a Linguist
			{
				cout << "\nYou encounter a Linguist. Your english skills have improved, increasing your luck by one!\n";
				(*iter)->GameObject::removeNPC();
				m_vpGameObjects.front()->GameObject::LinguistBehaviour();
			}

			else if ((*iter)->returnType()[0] == 'B') //If the first letter of the string is a 'B', it is a Blacksmith
			{
				cout << "\nYou encounter a Backsmith. Your attack power has been increased by 10!\n";
				(*iter)->GameObject::removeNPC();
				m_vpGameObjects.front()->GameObject::BlacksmithBehaviour();
			}

			else if ((*iter)->returnType()[0] == 'A') //If the first letter of the string is a 'A', it is a Armorer
			{
				cout << "\nYou encounter an Armorer. Your defence has been increased by 2!\n";
				(*iter)->GameObject::removeNPC();
				m_vpGameObjects.front()->GameObject::ArmorerBehaviour();
			}
			else
			{
				cout << "\nThe player and " << (*iter)->returnType() << " fight!\n";

				//Loop while player or enemy's health is greater than 0
				while ((m_vpGameObjects.front()->returnHealth() > 0) && ((*iter)->returnHealth() > 0))
				{
					//----------ENEMY'S TURN-------------
					//If the enemy is of type Treye
					if ((*iter)->returnType()[0] == 'T')
					{
						enemyVar(1); //Run enemyVar function and pass in 1 to let it know what enemy it is
						treyesEncountered++; //Used for win conditions. When three of each enemy type have been killed the player wins
					}
					//If the enemy is of type Sphero
					else if ((*iter)->returnType()[0] == 'S')
					{
						enemyVar(2);
						spherosEncountered++;
					}
					//If the enemy is of type Rubicks
					else
					{
						enemyVar(3);
						rubicksEncountered++;
					}

					//Create random number between 0 and 99 for chaining
					int chainNum = rand();
					int chainChance = (chainNum % 100); 

					//Enemy deals damage to player equal to the enemy's attack power minus the player's defence
					int eDamage = ((*iter)->returnAttack() - m_vpGameObjects.front()->returnDefence());

					//If the chain number is less than the objects luck, then chain
					if (chainChance < m_vpGameObjects.front()->returnLuck())
					{
						cout << "\nCHAIN! You has successfully chained, doubling your damage!\n\n";
						eDamage += eDamage; //Chaining doubles the damage you do for a turn
					}

					//Deal damage to player with the attack function. Damage dealt has already been calculated in eDamage
					m_vpGameObjects.front()->attack(eDamage);
					cout << "\n" << (*iter)->returnType() << " attacks! It deals " << eDamage << " damage!\n";

					m_vpGameObjects.front()->info();

					//----------PLAYER'S TURN-------------
					//Take in player input and save it as the player word
					cin >> playerWord;

					//Convert player input to uppercase so it can be compared correctly
					for (int i = 0; i < playerWord.size(); i++) //Run for loop for the length of the string
					{
						playerWord[i] = toupper(playerWord[i]); //Convert each individual character in the string to uppercase
					}

					//Call checkWord to determine word damage 
					float wordDamage = checkWord(playerWord, words);

					//Player deals damage to enemy equal to their attack power multipied by 1.x times the length of the string x, minus the enemies defence
					int pDamage = (m_vpGameObjects.front()->returnAttack() * ((wordDamage /10) + 1)) - (*iter)->returnDefence();

					//If the chain number is less than the objects luck, then chain
					if (chainChance < m_vpGameObjects.front()->returnLuck())
					{
						cout << "\nCHAIN! You has successfully chained, doubling your damage!\n\n";
						pDamage += pDamage;
					}

					//Deal damage to the enemy with the attack function. Damage dealt has already been calculated in pDamage
					(*iter)->attack(pDamage);
					cout << "\n" << "The player attacks! They deal " << pDamage << " damage!\n";

					(*iter)->info();

					//Losing condition. If the players health is less than or equals to 0 return true to main.cpp, ending the game
					if (m_vpGameObjects.front()->returnHealth() <= 0)
					{
						cout << "\n\nYou have died! Game Over!\n\n";
						return true;
					}

					//Winning condition. If the player has defeated 3 of each type of enemy return true to main.cpp, ending the game
					else if (treyesEncountered >= 3 && spherosEncountered >= 3 && rubicksEncountered >= 3)
					{
						cout << "\n\nCongratulations! You have survived! You have proved yourself worthy to return to the world of magic and things.\n\n";
						return true;
					}

					//Kill the enemy if their health is less than 0. Increase player stats as a reward for winning the fight
					else if ((*iter)->returnHealth() <= 0)
					{
						int healthIncrease = (*iter)->returnMaxHealth() / 10; // Increase players max health by 10% of the enemy's max health
						int defenceIncrease = (*iter)->returnDefence() / 5; // Increase players defence by 20% of the enemy's defence
						int attackIncrease = (*iter)->returnAttack() / 10; // Increase players attack power by 10% of the enemy's attack power
						m_vpGameObjects.front()->increaseStats(healthIncrease, defenceIncrease, attackIncrease); //Call the increase stats set method for the player

						cout << "\n\nYou have defeated! " << (*iter)->returnType() << "! \nYour max health increases by " << healthIncrease << "! \nYour attack power increases by " << attackIncrease << "! \nYour defence increases by " << defenceIncrease << "! \nYour luck increases by 1!\n\n";
						return false; 
					}

				}
			}
		}
	}
}

void Game::info() //Runs info function from GameObject.cpp for each object
{
	// use iterator search through vector
	vector <GameObject*>::const_iterator iter;

	for (iter = m_vpGameObjects.begin(); iter != m_vpGameObjects.end(); iter++)
	{
		(*iter)->info();
	}
}

void Game::clean() //Uses iterator to determine if a game object is alive and deletes it if it isn't
{
	
	vector <GameObject*>::const_iterator iter;

	for (iter = m_vpGameObjects.begin(); iter != m_vpGameObjects.end();)
	{
		//if the isAlive function returns false, delete the current object the iterator is pointing to. Otherwise, increment the iterator to view the next object in the vector
		if (!(*iter)->isAlive())
		{
			iter = m_vpGameObjects.erase(iter);
		}
		else
		{
			iter++;
		}
	}
	
}

//Compare the player's input to the words in the vector
int Game::checkWord(string playerWord, vector<string>words)
{
	if (find(words.begin(), words.end(), playerWord) != words.end())//Search through words vector from beginning to end, looking for the players word
	{
		cout << "You use " << playerWord << "! It's super effective! You do 1." << playerWord.length() << " times damage!";
		return playerWord.length(); //Return the length of the string as an int
	}
	else
	{
		cout << "That's not a valid word. You deal 1 times damage.\n";
		return 1; //If the player doesn't want to search for the word, you can enter any key and do standard damage
	}
}

vector<int> Game::createNums(int whichEnemy) //Creates random numbers and returns them for use with the enemy objects
{
	vector<int> randNums; //Vector of ints

	//Set random number equal to result of random function, but restrict it to values under 30. Add 1 or 3 to result as needed
	int randX = rand(); 
	int enemyX = (randX % 29) + 1;	
	randNums.push_back(enemyX); //Push results onto vector

	int randY = rand();
	int enemyY = (randY % 29) + 1;
	randNums.push_back(enemyY); 
	
	//Treye Stats
	if (whichEnemy == 1) //Use number passed in to determine what enemy is being used
	{
		//Treye health must between 50-150
		int health = rand(); 
		int enemyH = ((health % 11) + 5) * 10;	
		randNums.push_back(enemyH);

		//Treye attack power must be between 15-25
		int attackPower = rand();
		int enemyA = (attackPower % 11) + 15;
		randNums.push_back(enemyA);

		//Treye defence must be between 0-5
		int defence = rand();
		int enemyD = defence % 6;
		randNums.push_back(enemyD);

		//Treye luck must be between 1-3
		int luck = rand();
		int enemyL = (luck % 3) + 1;
		randNums.push_back(enemyL);
	}

	//Sphero Stats
	if (whichEnemy == 2)
	{
		//Sphero health must be between 150-250
		int health = rand();
		int enemyH = ((health % 11) + 15) * 10;
		randNums.push_back(enemyH);

		//Sphero attack power must be between 25-35
		int attackPower = rand();
		int enemyA = (attackPower % 11) + 25;
		randNums.push_back(enemyA);

		//Sphero defence must be between 5-10
		int defence = rand();
		int enemyD = (defence % 6) + 5;
		randNums.push_back(enemyD);

		//Sphero luck must be between 2-4
		int luck = rand();
		int enemyL = ((luck % 3) + 2);
		randNums.push_back(enemyL);
	}

	//Rubicks Stats
	if (whichEnemy == 3)
	{
		//Rubicks health must be between 250-350
		int health = rand();
		int enemyH = ((health % 11) + 25) * 10;
		randNums.push_back(enemyH);

		//Rubicks attack power must be between 35-50
		int attackPower = rand();
		int enemyA = (attackPower % 16) + 35;
		randNums.push_back(enemyA);

		//Rubicks defence must be between 10-15
		int defence = rand();
		int enemyD = (defence % 6) + 10;
		randNums.push_back(enemyD);

		//Rubicks luck must be between 3-5
		int luck = rand();
		int enemyL = ((luck % 3) + 3);
		randNums.push_back(enemyL);
	}

	whichEnemy = 0;
	return randNums;
}

vector<string> Game::enemyVar(int whichEnemy)
{
	if (whichEnemy == 1) //Use number passed in to determine what enemy is being used
	{
		int chooseWords = rand();
		int chosenWords = (chooseWords % 5); //Randomises enemy variation to be used
		printEnemy(chosenWords); //Call the printEnemy function with the random variable
		return wordList[chosenWords]; //return the set of words to be used
	}

	if (whichEnemy == 2)
	{
		int chooseWords = rand();
		int chosenWords = (chooseWords % 5) + 5;
		printEnemy(chosenWords);
		return wordList[chosenWords];
	}

	if (whichEnemy == 3)
	{
		int chooseWords = rand();
		int chosenWords = (chooseWords % 5) + 10;
		printEnemy(chosenWords);
		return wordList[chosenWords];
	}

	whichEnemy = 0;
}
//---------------------ASCII ART---------------------------
//Print the enemy quote and corresponding ASCII art
//Note: ASCII art looks strange here due to the formatting being different in the command line
void Game:: printEnemy(int whichEnemy)
{
	if (whichEnemy == 0)// Treye v1
	{
		cout << "\nHello there FRIEND...\n\n"; 

		cout << "                   m\n";
		cout << "                  fsf\n";
		cout << "                 dhshj\n";
		cout << "                edryykd\n";
		cout << "	       qwertymui\n";
		cout << "              dghjktljlhg\n";
		cout << "             fghjenduhnljm\n";
		cout << "            bfgfopfklsgdpfg\n";
		cout << "           searthsyjtjdfjkgj\n";
		cout << "          sdftfa/     \\kpdsfg\n";
		cout << "         tfvgsjk   o   jsynjmi\n";
		cout << "        dpokijuf\\     /k,lkdgxf\n";
		cout << "       dfghiuhjmoksendofpi,lkwlr\n";
		cout << "      sutruthofkdmkfkgpretendmidk\n";
		cout << "     dosuretfgwhtrendjmjutdbendokl\n";
		cout << "    driugijokpjwpoke,rlkgjodif,gldf\n";
	}

	if (whichEnemy == 1) // Treye v2
	{
		cout << "\nWhere's the LOVE?..\n\n";

		cout << "                   m\n";
        cout << "                  xof\n";
        cout << "                 dhshj\n";
        cout << "                edryykd\n";
		cout << "	       dovetymui\n";
  		cout << "              dghjktljlhg\n";
        cout << "             fghjenabovejm\n";
        cout << "            bfgfopfklsgdpfg\n";
        cout << "           searthsyjtjdfjkgj\n";
        cout << "          sdftfa/     \\kpdsfg\n";
        cout << "         tfvgsjk   o   jsynjmi\n";
        cout << "        dpokijuf\\     /kglovexf\n";
        cout << "       dfghiuhjmokdfttippi,lkwlr\n";
        cout << "      sutruthshovekfkgapwqiromidk\n";
        cout << "     doplalaobwh[el[pl.g[;dbendokl\n";
        cout << "    driugijokpjwpoke,rlkgjodif,gldf\n";
	}

	if (whichEnemy == 2) // Treye v3
	{
		cout << "\nAll I want is a HUG...\n\n";

		cout << "                   m\n";
        cout << "                  xgi\n";
        cout << "                 dhshj\n";
        cout << "                edryykd\n";
		cout << "               lioutymui\n";
  		cout << "              dghjkunplug\n";
        cout << "             fgdugqgetimjm\n";
        cout << "            bfgfopfklsgdpfg\n";
        cout << "           searthsyjtjdfjkgj\n";
        cout << "          sdftfa/     \\kpdsfg\n";
        cout << "         tfvgsjk   o   jsynjmi\n";
        cout << "        dpokijuf\\     /kfortoxf\n";
        cout << "       dfghiuhjmokdthugppi,lkwlr\n";
        cout << "      shrugaowtyrukfkgapwqiromidk\n";
        cout << "     doplalaobwhakjdfajdfvfbendokl\n";
        cout << "    driugijokdrugokearlkmjdhytkgldf\n";
	}

	if (whichEnemy == 3) // Treye v4
	{
		cout << "\nI feel so ALONE...\n\n";

		cout << "                   m\n";
        cout << "                  xgi\n";
        cout << "                 dhshj\n";
        cout << "                edryykd\n";
		cout << "               lioutymui\n";
  		cout << "              dghjkqjudte\n";
        cout << "             fgjimqgetimjm\n";
        cout << "            bfgfopfklsgdpfg\n";
        cout << "           swtonesyjtjdfjkgj\n";
        cout << "          sdftfa/     \\kpdsfg\n";
        cout << "         sconejk   o   jsynjmi\n";
        cout << "        dpokijuf\\     /kphonexf\n";
        cout << "       dfghiuhjmokdcrubppi,lkwlr\n";
        cout << "      qecytaowtyclonegapwqiromidk\n";
        cout << "     doplbonealaobwdnfsasdfbailokl\n";
        cout << "    driugijbilsokasearlsmjdhytkgldf\n";
	}

	if (whichEnemy == 4) // Treye v5
	{
		cout << "\nWhy can't we just talk about our feelings and STUFF?..\n\n";

		cout << "                   m\n";
        cout << "                  xgi\n";
        cout << "                 dpuff\n";
        cout << "                edryykd\n";
		cout << "               lioutymui\n";
  		cout << "              dghjkqjudte\n";
        cout << "             fgjimroughmjm\n";
        cout << "            bfgfopfklsgdpfg\n";
        cout << "           swyuivsyjtjdfjkgj\n";
        cout << "          sdftfa/     \\kpdsfg\n";
        cout << "         sconejk   o   jsynjmi\n";
        cout << "        dpokijuf\\     /kmsjkdxf\n";
        cout << "       dfghiutoughdcrubppi,lkwlr\n";
        cout << "      qecytaowtypapeygapwqbluffdk\n";
        cout << "     doplttrealaobwdnbuffsytailokl\n";
        cout << "    driugijbilsokasearlsmjdhytkgldf\n";
	}

	if (whichEnemy == 5) // Sphero v1
	{
		cout << "\nI'm on the highway to HELL! \n\n";

		cout << "        ,i,                                      ,i,\n";
		cout << "       fkjus                                    dbdyi\n";
		cout << "     fusyfh,                                    ,yuqisk\n";
		cout << "    aywiepddq        ,dfbkmlnkudjnfdmm,        qjaywbciq\n";
		cout << "     fkahyesrr  ,kshsurfbjnksieyfofdmmxdbg,  srudwellqj\n";
		cout << "      lkdufig'gdqxvbdfbdfbdfbd,awskwowtcdhYbg'lkdufigg\n";
		cout << "       'jjuk,djtyawsmellscmksjdnciuaehiaugytdq,butygq'\n";
		cout << "         fgazlkzocuwtgdbcusybwbdyxjwnsijehuznuqmqagt\n";
		cout << "         ,wkjandckjncadqa'         `'sdcswefvfbgbfr,\n";
		cout << "        ,apkkx,akmzza'                 'kjnanasiuwnc,\n";
		cout << "       ,ahdcbajhbcc'                     'apimxaiqwun,\n";
		cout << "       aksjndajsnd'        ,gasdg,        `ajjbqytqwbx\n";
		cout << "       lzkmmliqyha        gm'   `gb        qosuwtbxxxv\n";
		cout << "       acybechbacc        fe     kl        qiurtrgdrzb\n";
		cout << "       weiuffbsgsa        gr     it        alas,xaunas\n";
		cout << "       aksjdgdyncs,        'asell'        ,ap,paoaoijx\n";
		cout << "       asjhdbahsdaaa                     qpoiqwudjsteg\n";
		cout << "       `aehqyakwiyetsq                 aakshsgqyqusnx'\n";
		cout << "        `adciuqejncgddYb,_         _,dasxkjnauysgsfd'\n";
		cout << "         `iausnxyuaciqauhnchgsvdrfcsvdtgediunoasijc'\n";
		cout << "          `aocyenchyxncehjwnchxmzkmdcxjzmdfellbyt'\n";
		cout << "            'aduhbcuahejncueyhquydocdkosdihshdya'\n";
		cout << "              `askhgquteyhcibellhzguyshdcjmaoa'\n";
		cout << "                 `aduqhuwyhnahsctcnnvhvgrfd'\n";
		cout << "                      `ashqnefuhiybauaa'\n";
	}

	if (whichEnemy == 6) // Sphero v2
	{
		cout << "\nA, B, C, It's easy as, one, two, THREE! \n\n";

		cout << "        ,i,                                      ,i,\n";
		cout << "       fkjus                                    dbdyi\n";
		cout << "     fusyfh,                                    ,yuqisk\n";
		cout << "    aywiepddq        ,dfbkmlnkudjnfdmm,        qjaywbciq\n";
		cout << "     fkahyesrr  ,kshsurfbjnksieyfofdmmxdbg,  sruhjuyfqj\n";
		cout << "      lkdufig'gdPxvbdfbdfbdfbd,awskwowtcdhYbg'lkdufigg\n";
		cout << "       'jjuk,dkauitnmwooscmksjdnciuaagreegytdq,butygq'\n";
		cout << "         fgazlkzocuwtgdbcusybwbdyxjwnsijehuznuqmqagt\n";
		cout << "         ,wkjandckjncadPa'         `'sdcswefvfbgbfr,\n";
		cout << "        ,apkkx,akmzza'                 'kjnanasiuwnc,\n";
		cout << "       ,ahdcbajhbcc'                     'apimxaiqwun,\n";
		cout << "       aksjndajsnd'        ,gabee,        `ajjbqytqwbx\n";
		cout << "       lzkmmliqyha        gm'   `gb        qosuwtbxxxv\n";
		cout << "       acybechbacc        fe     kl        qiurtrgdrzb\n";
		cout << "       weiuffbsgsa        gr     it        alas,xaunas\n";
		cout << "       aksjdgdyncs,        'aajkt'        ,ap,paoaoijx\n";
		cout << "       asjhdbahsdaaa                     qpoiqwudjsteg\n";
		cout << "       `aehqyakwiyetsq                 aakshsgqyqusnx'\n";
		cout << "        `adciufleecgddYb,_         _,dasxkjnauysgsfd'\n";
		cout << "         `iausnxyuaciqauhnchgsvdrfcsvdtgediunoasijc'\n";
		cout << "          `aocyenchyxncehjwnchxmzkmdcxjzmdpoudbyt'\n";
		cout << "            'aduhbcuahejncueyhquydocdkosfreehdya'\n";
		cout << "              `askhgquteyhcimjghhzguyshdcjmaoa'\n";
		cout << "                 `aduqhuwyhnahscspreehvgrfd'\n";
		cout << "                      `ashqnefuhiybauaa'\n";
	}

	if (whichEnemy == 7) // Sphero v3
	{
		cout << "\nOh no, not I, I will SURVIVE!\n\n";

		cout << "        ,i,                                      ,i,\n";
		cout << "       fkjus                                    dbdyi\n";
		cout << "     fusyfh,                                    ,yuqisk\n";
		cout << "    aywiepddq        ,dfbkmlnkudjnfdmm,        qjaywbciq\n";
		cout << "     fkfivesrr  ,kshsurfbjnksieyfofdmmxdbg,  sruqlotvqj\n";
		cout << "      lkdufig'gdqxvbdfbdhivebd,awskwowtcdhYbg'lkdufigg\n";
		cout << "       'jjuk,dlopaubeqrscmksjdnciuaehiaugytdq,butygq'\n";
		cout << "         fgazlkzocuwtgdbcusybwbdyxjwnsijehuznuqmqagt\n";
		cout << "         ,wkjandckjncadna'         `'sdcswefvfbgbfr,\n";
		cout << "        ,apkkx,akmzza'                 'kjnanasiuwnc,\n";
		cout << "       ,ahdcbajhbcc'                     'apimxaiqwun,\n";
		cout << "       aksjndajsnd'        ,gasdg,        `ajjbqytqwbx\n";
		cout << "       lzkmmliqyha        gm'   `gb        qosuwtbxxxv\n";
		cout << "       acybechbacc        fe     kl        qiurtrgdrzb\n";
		cout << "       weiuffbsgsa        gr     it        alas,xaunas\n";
		cout << "       aksjdgdyncs,        'aitbm'        ,ap,paoaoijx\n";
		cout << "       asjhdbahsdaaa                     qpoiqwudjsteg\n";
		cout << "       `aehqdriveyetsq                 aakshsgthrivex'\n";
		cout << "        `adciuqejncgddYb,_         _,dasxkjnauysgsfd'\n";
		cout << "         `iausnxyuaciqauhnchgsvdrfcsvdtgediunoasijc'\n";
		cout << "          `aocyenchyxncehjwnchxmzkmdcxjzmdjthybyt'\n";
		cout << "            'aduhbcuahejncueyhquydocdkosdihshdya'\n";
		cout << "              `askhgqutedivejkiohzguyshdcjmaoa'\n";
		cout << "                 `aduqhuwyhnahsctcnnvhvgrfd'\n";
		cout << "                      `ashqnefuhiybauaa'\n";
	}

	if (whichEnemy == 8) // Sphero v4
	{
		cout << "\nYou are the dancing QUEEN! \n\n";

		cout << "        ,i,                                      ,i,\n";
		cout << "       fkjus                                    dbdyi\n";
		cout << "     meanfh,                                    ,yuqisk\n";
		cout << "    aywiepddq        ,dfbkmlnkudjnfdmm,        qjaywbciq\n";
		cout << "     fkahyesrr  ,kshsurfbjnksieyfofdmmxdbg,  sruqpmnhuqj\n";
		cout << "      lkdufig'gdqxvbdfbdfbdfbd,awskwowtcdhYbg'lakdufigg\n";
		cout << "       'jjuk,djtyawljcleanmksjdnciuaehiaugytdq,bcutygq'\n";
		cout << "         fgazlkzocuwtgdbcusybwbdyxjwnsijehuznxuqmqagt\n";
		cout << "         ,wkjandckjncadqa'         `'sdcswefvfbgbfr,\n";
		cout << "        ,apkkx,akmzza'                 'kjnanasiuwnc,\n";
		cout << "       ,ahdcbajhbcc'                     'apimxaiqwun,\n";
		cout << "       aksjndajsnd'        ,gasdg,        `ajjbqytqwbx\n";
		cout << "       lzkmmliqyha        gm'   `gb        qosgreenxxv\n";
		cout << "       acybechbacc        fe     kl        qiurtrgdrzb\n";
		cout << "       weiuffbsgsa        gr     it        alas,xaunas\n";
		cout << "       aksjdgdyncs,        'aawet'        ,ap,paoaoijx\n";
		cout << "       asjhdbahsdaaa                     qpoiqwudjsteg\n";
		cout << "       `aehqyakwiyetsq                 aakshsgqyqusnx'\n";
		cout << "        `adciuqejncgddYb,_         _,dasxkjnauysgsfd'\n";
		cout << "         `iausnxyuaciqauhscreendrfcsvdtgediseensijc'\n";
		cout << "          `aocyenchyxncehjwnchxmzkmdcxjzmderytbyt'\n";
		cout << "            'aduhbcuahejncueyhquydocdkosdihshdya'\n";
		cout << "              `askhgquteyhcijuethzguyshdcjmaoa'\n";
		cout << "                 `aduqhuwyhnahsctcnnvhvgrfd'\n";
		cout << "                      `ashqnefuhiybauaa'\n";
	}

	if (whichEnemy == 9) // Sphero v5
	{
		cout << "\nWe will we will rock YOU!\n\n";

		cout << "        ,i,                                      ,i,\n";
		cout << "       fkjus                                    dbdyi\n";
		cout << "     fusyfh,                                    ,yuqisk\n";
		cout << "    aywiepddq        ,dfbkmlnkudjnfdmm,        qjaywbciq\n";
		cout << "     fkahyesrr  ,kshsurfbjnksieyfofdmmxdbg,  sruqpmnhuqj\n";
		cout << "      lkdufig'gdqxvbdfbdfbdfbd,awskwowtcdhYbg'lakdufigg\n";
		cout << "       'jjuk,djtyawljhgdacmksjdnciuablueugytdq,bcutygq'\n";
		cout << "         fgazlkzocuwtgdbcusybwbdyxjwnsijehuznxuqmqagt\n";
		cout << "         ,wkjandckjncadqa'         `'sdcswefvfbgbfr,\n";
		cout << "        ,apkkx,akmzza'                 'kjnanasiuwnc,\n";
		cout << "       ,ahdcbajhbcc'                     'apimxaiqwun,\n";
		cout << "       aksjndajsnd'        ,gasdg,        `ajjbqytqwbx\n";
		cout << "       lzkmmliqyha        gm'   `gb        qosuwtbxxxv\n";
		cout << "       acybechbacc        fe     kl        qiurtrgdrzb\n";
		cout << "       weiuffbsgsa        gr     it        alas,xaunas\n";
		cout << "       aksjdgdyncs,        'aawet'        ,ap,paoaoijx\n";
		cout << "       asjhdbahsdaaa                     qpoiqwudjsteg\n";
		cout << "       `aehqyakgluetsq                 aakshsgqyqusnx'\n";
		cout << "        `adciuqejncgddYb,_         _,dasxkjnauysgsfd'\n";
		cout << "         `iausnxyuaciqauhnchgsvdrfcsvdtgediunoasijc'\n";
		cout << "          `aocyenchyxncehjwnchxmzkmdcxscrewrytbyt'\n";
		cout << "            'aduhbcuahejncueyhquydocdkosdihshdya'\n";
		cout << "              `askhgqueuehcijuethzguyshdcjmaoa'\n";
		cout << "                 `aduqhuwyhnahsctchewhvgrfd'\n";
		cout << "                      `ashqnefuhiybauaa'\n";
	}

	if (whichEnemy == 10) // Rubicks v1
	{
		cout << "\nPREPARE to defend yourself, you've never fought someone of my caliber! \n\n";

		cout << "	qwddertbnjuyigfyuio  yusiotpseesmaeulopt  qqagdsqlyqdtffehaph\n";
		cout << "	yqiznftbnjuyigywgiy  qwfodhqidnakktrquwt  pakdltjwamdjssxbaku\n";
		cout << "	aksajnvkjanskvneasv  yusfwwiopssmaewlopt  oaagxodhgrqlkancndh\n";
		cout << "	nnyudqf/   \\qpanchi  yxuhwfi/   \\lgquuop  kkmygia/   \\alkmcye\n";
		cout << "	qwdmhuh  o  tqybtra  mhbipia  o  opgtgeg  trwzfga  o  ishdywb\n";
		cout << "	ewpzahy\\   /yicdmgy  xubrgsq\\   /tvredsd  mkabdwa\\   /aasdugw\n";
		cout << "	sjvkslajfchwtcvnskd  oijabfrtaasdbrereew  osimydsdvoiqqetrjsn\n";
		cout << "	laksudrqqtjudarebko  yunittsmnsopmaloopk  xjarrglairjtdteapie\n";
		cout << "	qwkzjhsduyhnzkzxcuw  skdmksjndckmsndsfyr  sbagdskadtttettskjd\n";
		cout << "\n";
		cout << "	caksuidjoaksdknaufs  alkmsdiybnvsbcynxma  aksjdnckhshfjhnvzbv\n";
		cout << "	aosidiyenjxmaasdwwf  aokkmdiusndhcettqif  lzkcareuyehfubaasdf\n";
		cout << "	ouadvcnahstwdkxjsis  pasdomasciyfndghsta  qokfmsdhbusbnvsryhd\n";
		cout << "	askdjnk/   \\yitsuhi  yxuhwfi/   \\lgquuop  tsmygia/   \\gatsewd\n";
		cout << "	zxcioaa  o  poydfhd  khcxtaa  o  qoldhdy  jfimfje  o  mknjbhv\n";
		cout << "	qwpbddf\\   /asldkmy  kkurywm\\   /opredsd  miabdwa\\   /agyxrph\n";
		cout << "	piajiuncuyrqjsuetsm  skidjnnugqgriqosrhq  lckmskjdnvmnyfdddvx\n";
		cout << "	zxcmiadjsdieyzppsdd  pamosudnuyasdasdatw  kstqskcundsduyhfesa\n";
		cout << "	asodijdifyhajsnatwq  asdlkmsiudmvsiydtwq  ajjzahbcuhanbsuyasd\n";
		cout << "\n";
		cout << "	ayckmlaksiudhawtwdd  yusiotpseesmaeulopt  miagdsqlyqdtffehaph\n";
		cout << "	zlckmhsshiaflareugn  qwfodhqidnakkhairwt  pakdltjwamdjssxbaku\n";
		cout << "	aksajnvkjanskvneasv  yusfwwiopssmaewlopt  miagxodhgrqtewhhaph\n";
		cout << "	aspofyg/   \\sdjfnvh  wryrfnv/   \\gblodfd  tsmygia/   \\asfnvts\n";
		cout << "	qwdcdof  o  xcljvtw  siysnvx  o  aosdiau  askdjnc  o  asoifay\n";
		cout << "	zxkchby\\   /yicgtrq  fvjnuay\\   /asldkju  miabdwa\\   /vusdfbv\n";
		cout << "	zxlckmiaufmndfywrer  skidjnnugqgriqosrhq  osimydsdvoiqqetrjsn\n";
		cout << "	asdmiauhsnchaustwjj  rodufhutehjxajahsur  svhsbddjhnvsuytetwq\n";
		cout << "	qwerdtbfgnjuyiddgjy  yuniopsttsqsmalgopt  miagdskadtttetthaph\n";
	}

	if (whichEnemy == 11) // Rubicks v2
	{
		cout << "\nIt's already over for you... I have the HIGH ground!\n\n";

		cout << "	qwddertbnjuyigfyuio  yusiotpseesmaeulopt  qqagdsqlyqdtffehaph\n";
		cout << "	yqiznftbnjuyigywgiy  qwfodhqidnakktrquwt  pakdltjwamdjssxbaku\n";
		cout << "	aksajnvkjanskvneasv  yusfwwiopssmaewlopt  oaagxodhgrqlkancndh\n";
		cout << "	nnyudqf/   \\qpanchi  yxuhwfi/   \\lgquuop  kkmygia/   \\alkmcye\n";
		cout << "	qwdmhuh  o  tqybtra  mhbipia  o  opgtgeg  trwzfga  o  ishdywb\n";
		cout << "	ewpzahy\\   /yicdmgy  xubrgsq\\   /tvredsd  mkabdwa\\   /aasdugw\n";
		cout << "	sjvkslajfchwtcvnskd  oijabfrtaasdbrereew  osimydsdvoiqqetrjsn\n";
		cout << "	laksudrqqtjuyidfbko  yunittsmnsopmaloopk  xjarrasgshjtdteapie\n";
		cout << "	qwkzjhsduyhnzkzxcuw  skdmksjsighsndsdfyr  sbagdskadtttettskjd\n";
		cout << "\n";
		cout << "	caksuidjoaksdknaufs  alkmsdiybnvsbcynxma  aksjdnckhshfjhnvzbv\n";
		cout << "	aosidiyenjxcryadwwf  aokkmdiusndhcettqif  lzkmcisuyehfubaasdf\n";
		cout << "	ouadvcnahstwdkxjsis  pasdomasciyfndghsta  qokfmsdhbusbnvsryhd\n";
		cout << "	askdjnk/   \\yitsuhi  yxuhwfi/   \\lgquuop  tsmygia/   \\gatsewd\n";
		cout << "	zxcioaa  o  poydfhd  khcxtaa  o  qoldhdy  jfimfje  o  mknjbhv\n";
		cout << "	qwpbddf\\   /asldkmy  kkurywm\\   /opredsd  miabdwa\\   /agyxrph\n";
		cout << "	piajiuncuyrqjsuetsm  skidjnnugqgriqosrhq  lckmskjdnvmnyfdddvx\n";
		cout << "	zxcmiadjsdieyzppsdd  pamosudnuyasdasdatw  kstqskcunshyijhfesa\n";
		cout << "	asodijdifyhajsnatwq  asdlkmsiudmvsiydtwq  ajjzahbcuhanbsuyasd\n";
		cout << "\n";
		cout << "	ayckmlaksiudhawtwdd  yusiotpseesmaeulopt  miagdsqlyqdtffehaph\n";
		cout << "	zlckmhsshiaygwtdugn  qwfodhqidnakktrquwt  pakdltjwamdjssxbaku\n";
		cout << "	aksajnvkjanskvneasv  yureplyopssmaewlopt  miagxodhgrqtewhhaph\n";
		cout << "	aspofyg/   \\sdjfnvh  wryrfnv/   \\gblodfd  tsmygia/   \\asfnvts\n";
		cout << "	qwdcdof  o  xcljvtw  siysnvx  o  aosdiau  thighnc  o  asoifay\n";
		cout << "	zxkchby\\   /yicgtrq  fvjnuay\\   /asldkju  miabdwa\\   /vusdfbv\n";
		cout << "	zxlckmiaufmndfywrer  skidjnnugqgriqosrhq  osimydsdvoiqqetrjsn\n";
		cout << "	asdmiauhsnchaustwjj  rodufhutehjxajahsur  svhsbddjhnvsuytetwq\n";
		cout << "	qwerdtbfgnjuyiddgjy  yuniopsttsqsmalgopt  miagdskadtttetthaph\n";
	}

	if (whichEnemy == 12) // Rubicks v3
	{
		cout << "\nLet's FACE it, you're gonna die. Sorry...\n\n";

		cout << "	qwddertbnjuyigfyuio  yusiotpseesmaeulopt  qqagdsqlyqdtffehaph\n";
		cout << "	yqiznftbnjuyigywgiy  qwfodhqidnakktrquwt  pakdltjwamdjssxbaku\n";
		cout << "	aksajnvkjanskvneasv  yusfwwiopssmaewlopt  oaagxodhgrqlkancndh\n";
		cout << "	nnyudqf/   \\qpanchi  yxuhwfi/   \\lgquuop  kkmygia/   \\alkmcye\n";
		cout << "	qwdmhuh  o  tqybtra  mhbipia  o  opgtgeg  trwzfga  o  ishdywb\n";
		cout << "	ewpzahy\\   /yicdmgy  xubrgsq\\   /tvredsd  mkabdwa\\   /aasdugw\n";
		cout << "	sjvkslajfchwtcvnskd  oijabfrtaasdbrereew  osimydsdvoiqqetrjsn\n";
		cout << "	laksudrqqtjuyidfbko  yunittsmnsopmaloopk  xjarrasgshjtdteapie\n";
		cout << "	qwkzjhsduyhnzkzxcuw  skdmksjndckmsndsfyr  sbagdskadtttettskjd\n";
		cout << "\n";
		cout << "	caksuidjoaksdkplace  alkmsdiybnvsbcynxma  aksjdnckhshfjhnvzbv\n";
		cout << "	aosidiyenjxmaasdwwf  aokkmdiusndhcettqif  lzkmcisuyehfubaasdf\n";
		cout << "	ouadvcnahstwdkxjsis  pasdomasciyfndghsta  qokfmsdhbusbnvsryhd\n";
		cout << "	askdjnk/   \\yitsuhi  yxuhwfi/   \\lgquuop  tsmygia/   \\gatsewd\n";
		cout << "	zxcioaa  o  poydfhd  khcxtaa  o  qoldhdy  jfimfje  o  mknjbhv\n";
		cout << "	qwpbddf\\   /asldkmy  kkurywm\\   /opredsd  miabdwa\\   /agyxrph\n";
		cout << "	piajiuncuyrqjsuetsm  skidjnnugqgriqosrhq  lckmskjdnvmnyfdddvx\n";
		cout << "	zxcmiadjsdieyzppsdd  pamosudnuyasdasdatw  kstqskcundsduyhfesa\n";
		cout << "	asodijdifyhajsnatwq  asdlkmsiudmvsiydtwq  ajracebcuhanbsuyasd\n";
		cout << "\n";
		cout << "	ayckmlaksiudhawtwdd  yusiotpseesmaeulopt  miagdsqlyqdtffehaph\n";
		cout << "	zlckmhsshiaygwtdugn  qwfodhqidnakktrquwt  pakdltjwacasefxbaku\n";
		cout << "	aksajnvkjanskvneasv  yusfwwiopssmaewlopt  miagxodhgrqtewhhaph\n";
		cout << "	aspofyg/   \\sdjfnvh  wryrfnv/   \\gblodfd  tsmygia/   \\asfnvts\n";
		cout << "	qwdbase  o  xcljvtw  siysnvx  o  aosdiau  askdjnc  o  asoifay\n";
		cout << "	zxkchby\\   /yicgtrq  fvjnuay\\   /asldkju  miabdwa\\   /vusdfbv\n";
		cout << "	zxlckmiaufmndfywrer  skidjnnugqgriqosrhq  osimydsdvoiqqetrjsn\n";
		cout << "	asdmiauhsnchaustwjj  rodufhutehjxchaseur  svhsbddjhnvsuytetwq\n";
		cout << "	qwerdtbfgnjuyiddgjy  yuniopsttsqsmalgopt  miagdskadtttetthaph\n";
	}

	if (whichEnemy == 13) // Rubicks v4
	{
		cout << "\nI am fuelled by the PAIN of a thousand stubbed toes! Have at you!\n\n";

		cout << "	qwddertbnjuyigfyuio  yusiotpseesmaeulopt  qqagdsqlyqdtffehaph\n";
		cout << "	yqiznftbnjuyigywgiy  qrainhqidnakktrquwt  pakdltjwamdjssxbaku\n";
		cout << "	aksajnvkjanskvneasv  yusfwwiopssmaewlopt  oaagxodhgrqlkancndh\n";
		cout << "	nnyudqf/   \\qpanchi  yxuhwfi/   \\lgquuop  kkmygia/   \\alkmcye\n";
		cout << "	qwdmhuh  o  tqybtra  mhbipia  o  opgtgeg  trwzfga  o  ishdywb\n";
		cout << "	ewpzahy\\   /yicdmgy  xubrgsq\\   /tvredsd  mkabdwa\\   /aasdugw\n";
		cout << "	sjvkslajfchwtcvnskd  oijabfrtaasdbrereew  osimydsdvoiqqetrjsn\n";
		cout << "	laksudrqqtjuyidfbko  yunbanemnsopmaloopk  xjarraschaindteapie\n";
		cout << "	qwkzjhsduyhnzkzxcuw  skdmksjndckmsndsfyr  sbagdskadtttettskjd\n";
		cout << "\n";
		cout << "	caksuidjoaksdknaufs  alkmsdiybnvsbcynxma  aksjdnckhshfjhnvzbv\n";
		cout << "	aosidiyenjxmaasdwwf  aokkmdiusndhcettqif  lzkmcisuyehfubaasdf\n";
		cout << "	ouadvcnahstwdkxjsis  pasdomasciyfndghsta  qokfmsdhbusbnvsryhd\n";
		cout << "	askdjnk/   \\yitsuhi  yxuhwfi/   \\lgquuop  tsmygia/   \\gatsewd\n";
		cout << "	zxcioaa  o  poydfhd  khcxtaa  o  qoldhdy  jfimfje  o  mknjbhv\n";
		cout << "	qwpbddf\\   /asldkmy  kkurywm\\   /opredsd  miabdwa\\   /agyxrph\n";
		cout << "	piajiuncgainjsuetsm  skidjnnugqgriqosrhq  lckmskjdnvmnyfdddvx\n";
		cout << "	zxcmiadjsdieyzppsdd  pamosudnuyasdasdatw  kstqskcundsduyhfesa\n";
		cout << "	asodijdifyhajsnatwq  asdlkmsiudmvsiydtwq  ajjzahbcuhanbsuyasd\n";
		cout << "\n";
		cout << "	ayckmlaksiudhawtwdd  yusiotpseesmaeulopt  miagdsqlyqdtffehaph\n";
		cout << "	zlckmhsshiaygwtdugn  qwfodhqidnakktrquwt  pakdltjwamdjssxbaku\n";
		cout << "	aksajnvkjanskvneasv  yuslainopssmaewlopt  miagxodhgrqtewhhaph\n";
		cout << "	aspofyg/   \\sdjfnvh  wryrfnv/   \\gblodfd  tsmygia/   \\asfnvts\n";
		cout << "	qwdcdof  o  xcljvtw  siysnvx  o  aosdiau  askdjnc  o  asoifay\n";
		cout << "	zxkchby\\   /yicgtrq  fvjnuay\\   /asldkju  miabdwa\\   /vusdfbv\n";
		cout << "	zxlckmiaufmndfywrer  skidjnnugqgriqosrhq  osimydsdvoiqqetrjsn\n";
		cout << "	asdmiauhsnchaustwjj  rodufhutehjxajahsur  svhsbddjhnvsuytetwq\n";
		cout << "	qwerdtbfgnjuyiddgjy  yuniopsttsqsmalgopt  miagdskadtttetthaph\n";
	}

	if (whichEnemy == 14) // Rubicks v5
	{
		cout << "\nI am the very BEST, like no one ever was! You don't stand a chance...\n\n";

		cout << "	westertbnjuyigfyuio  yusiotpseesmaeulopt  qqagdsqlyqdtffehaph\n";
		cout << "	yqiznftbnjuyigywgiy  qwfodhqidnakktrquwt  pakdltjwamdjssxbaku\n";
		cout << "	aksajnvkjanskvneasv  yusfwwiopssmaewlopt  oaagxodhgrqlkancndh\n";
		cout << "	nnyudqf/   \\qpanchi  yxuhwfi/   \\lgquuop  kkmygia/   \\alkmcye\n";
		cout << "	qwdmhuh  o  tqybtra  mhbipia  o  opgtgeg  trwzfga  o  ishdywb\n";
		cout << "	ewpzahy\\   /yicdmgy  xubrgsq\\   /tvredsd  mkabdwa\\   /aasdugw\n";
		cout << "	sjvkslajfchwtcvnskd  oijabfrtaasdbrereew  osimydsdvoiqqetrjsn\n";
		cout << "	laksudrqqtjuyidfbko  yunittsmnsopmaloopk  xjarrasgshjtdteapie\n";
		cout << "	qwkzjhsduyhnzkzxcuw  skdmksjndckmsndsfyr  sbagdskadtttettskjd\n";
		cout << "\n";
		cout << "	caksuidjoaksdknaufs  alkmsdiybnvsbcynxma  aksjdnckhshfjhnvzbv\n";
		cout << "	aoquestenjxmaasdwwf  aokkmtestndhcettqif  lzkmcisuyehfubaasdf\n";
		cout << "	ouadvcnahstwdkxjsis  pasdomasciyfndghsta  qokfmsdhbusbnvsryhd\n";
		cout << "	askdjnk/   \\yitsuhi  yxuhwfi/   \\lgquuop  tsmygia/   \\gatsewd\n";
		cout << "	zxcioaa  o  poydfhd  khcxtaa  o  qoldhdy  jfimfje  o  mknjbhv\n";
		cout << "	qwpbddf\\   /asldkmy  kkurywm\\   /opredsd  miabdwa\\   /agyxrph\n";
		cout << "	piajiuncuyrqjsuetsm  skidjnnugqgriqosrhq  lckmskjdnvmnyfdddvx\n";
		cout << "	zxcmiadjsdieyzppsdd  pamosudnuyasdasdatw  kstqskcundsduyhfesa\n";
		cout << "	asodijdifyhajsnatwq  asdlkmsiguestiydtwq  ajjzahbcuhanbsuyasd\n";
		cout << "\n";
		cout << "	ayckmlaksiudhawtwdd  yusiotpseesmaeulopt  miagdsqlyqdtffehaph\n";
		cout << "	zlckmhsshiaygwtdugn  qwfodhqidnakktrquwt  pakdltjwamdjssxbaku\n";
		cout << "	aksajnvkjanskvneasv  yusfwwiopssmaewlopt  miagxodhgrqtewhhaph\n";
		cout << "	aspofyg/   \\vestnvh  wryrfnv/   \\gblodfd  tsmygia/   \\asfnvts\n";
		cout << "	qwdcdof  o  xcljvtw  siysnvx  o  aosdiau  askdjnc  o  asoifay\n";
		cout << "	zxkchby\\   /yicgtrq  fvjnuay\\   /asldkju  miabdwa\\   /vusdfbv\n";
		cout << "	zxlckmiaufmndfywrer  skidjnnugqgriqosrhq  osimydsdvoiqqetrjsn\n";
		cout << "	asdmiauhsnchaustwjj  rodufhutehjxajahsur  svhsbddjhnvsuytetwq\n";
		cout << "	qwerdtbfgnjuyiddgjy  yuniopsttsqsmalgopt  miagdskadtttetthaph\n";
	}
	
	whichEnemy = 0;
}