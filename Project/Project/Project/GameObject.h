#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include <vector>
#include <iostream>
#include <string>

using namespace std;

class GameObject
{
public:
	GameObject(); //Constructor
	~GameObject(); //Destructor

	void spawn(string typeID, int health, int speed, int x, int y, int attackPower, int defence, int luck); // create an object
	void draw(); // print to the screen the typeID and its x and y coords
	virtual void update(); //virtual function
	void info(); // print all info relating to the object
	void makeRandomPun(); // prints random pun to screen
	bool isAlive(); //return true if its  health is greater than 0

	//Getters. Retrive the appropriate member variables for a given game object
	string returnType();
	int returnX();
	int returnY();
	int returnMaxHealth();
	int returnHealth();
	int returnAttack();
	int returnDefence();
	int returnLuck();

	//Setters. Change the appropriate member variables for a given game object
	void attack(int damageDealt);
	void removeNPC();
	void HealerBehaviour();
	void LinguistBehaviour();
	void BlacksmithBehaviour();
	void ArmorerBehaviour();
	void increaseStats(int healthIncrease, int defenceIncrease, int attackIncrease);

protected:

	//Member variables
	string m_typeID;
	int m_maxHealth;
	int m_health;
	int m_speed;
	int m_x;
	int m_y;
	int m_attack;
	int m_defence;
	int m_luck;
};

#endif // !GAMEOBJECT_H