#include "GameObject.h"

//Default constructor and destructor
GameObject::GameObject()
{
}

GameObject::~GameObject()
{
}

void GameObject::spawn(string typeID, int maxHealth, int speed, int x, int y, int attackPower, int defence, int luck) //Sets values passed in to member variables to be used elsewhere
{
	m_typeID = typeID;
	m_maxHealth = maxHealth;
	m_health = maxHealth;
	m_speed = speed;
	m_x = x;
	m_y = y;
	m_attack = attackPower;
	m_defence = defence;
	m_luck = luck;
}

void GameObject::draw() //Prints X and Y coordinates alongside the TypeID
{
	//cout << "Type ID: " << m_typeID << " | X: " << m_x << " | Y: " << m_y << " |\n";
}

void GameObject::update() //Virtual function
{	
}

void GameObject::info() //Print game objects stats
{
	cout << "\n\nName: " << m_typeID << " \nCurrent Health: " << m_health << "\nMax Health: " << m_maxHealth << " \nAttack Power: " << m_attack << " \nDefence: " << m_defence << " \nLuck: " << m_luck << "\n\n";
}

void GameObject::makeRandomPun() //Prints random pun to screen
{
	int randomNum = rand();
	int randomPun = (randomNum % 10);

	if (randomPun == 0)
	{
		cout << "It doesn't matter how much you push the envelope. It'll still be stationary.\n\n";
	}
	else if (randomPun == 1)
	{
		cout << "I walked into my sister's room and tripped on a bra. It was a booby-trap.\n\n";
	}
	else if (randomPun == 2)
	{
		cout << "A book just fell on my head, I only have my shelf to blame.\n\n";
	}
	else if (randomPun == 3)
	{
		cout << "I stayed up all night wondering where the sun went. Then it dawned on me.\n\n";
	}
	else if (randomPun == 4)
	{
		cout << "Why did the scarecrow get a promotion? He was outstanding in his field.\n\n";
	}
	else if (randomPun == 5)
	{
		cout << "When a clock is hungry... It goes back four seconds.\n\n";
	}
	else if (randomPun == 6)
	{
		cout << "The frustrated cannibal threw up his hands.\n\n";
	}
	else if (randomPun == 7)
	{
		cout << "Diarrhea is hereditary... It runs in your genes.\n\n";
	}
	else if (randomPun == 8)
	{
		cout << "It's not that the guy didn't know how to juggle... He just didn't have the balls to do it.\n\n";
	}
	else if (randomPun == 9)
	{
		cout << "What did the cannibal get when he showed up to the party late? A cold shoulder!\n\n";
	}
}

bool GameObject::isAlive() //Checks if an objects health is 0 or less and returns false if it is
{
	if (m_health == 10000) //Used to remove NPC's 
	{
		return false;
	}
	else if (m_health > 0) //Used to remove player/enemies
	{
		return true;
	}
	
	else
	{
		cout << "Oh no. I, " << m_typeID << " have died. I am  mildly upset.\n";
		cout << m_typeID << "'s tombstone reads: ";
		makeRandomPun();
		return false;
	}
}

	//Getter methods. Retrive the appropriate member variables for a given game object
	string GameObject::returnType()
	{
		return m_typeID;
	}
	int GameObject::returnX()
	{
		return m_x;
	}
	int GameObject::returnY()
	{
		return m_y;
	}
	int GameObject::returnMaxHealth()
	{
		return m_maxHealth;
	}
	int GameObject::returnHealth()
	{
		return m_health;
	}
	int GameObject::returnAttack()
	{
		return m_attack;
	}
	int GameObject::returnDefence()
	{
		return m_defence;
	}
	int GameObject::returnLuck()
	{
		return m_luck;
	}

	//Set methods. Change the appropriate member variables for a given game object
	//Reduce health for game object depending on number passed in
	void GameObject::attack(int damageDealt)
	{
		m_health -= damageDealt;
	}

	//Set the health of the NPC to 10000. This is used in the clean function to remove the NPC without printing a message that it has been killed by the player
	void GameObject::removeNPC()
	{
		m_health = 10000;
	}

	//Increase max health of the player and restore the current health to the max health
	void GameObject::HealerBehaviour()
	{
		cout << "\nYour max health was: " << m_maxHealth << ". Your current health was: " << m_health;
		m_maxHealth += 50;
		m_health = m_maxHealth;
		cout << "\nYour max health is now: " << m_maxHealth << ". Your current health is: " << m_health << "\n\n";
	}

	//Adds one point of luck to the player
	void GameObject::LinguistBehaviour()
	{
		m_luck++;
	}

	//Add 10 to the players attack power
	void GameObject::BlacksmithBehaviour()
	{
		cout << "\nYour attack power was: " << m_attack;
		m_attack += 10;
		cout << "\nYour attack power is now: " << m_attack << "\n\n";
	}

	//Add 2 to the players defence
	void GameObject::ArmorerBehaviour()
	{
		cout << "\nYour defence was: " << m_defence;
		m_defence += 2;
		cout << "\nYour defence is now: " << m_defence << "\n\n";
	}

	// Increase the stats of the player with the passed in values
	void GameObject::increaseStats(int healthIncrease, int defenceIncrease, int attackIncrease)
	{
		m_maxHealth += healthIncrease;
		m_defence += defenceIncrease;
		m_attack += attackIncrease;
		m_luck++;
	}
